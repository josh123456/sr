# backend/main.py

import io
import os
import re
from typing import Optional

import uvicorn
from PIL import Image
from cv2 import cv2
from fastapi import FastAPI, File, Form, UploadFile, BackgroundTasks, Response, status
from fastapi.middleware.cors import CORSMiddleware
from ml_sr.api.enhancer import Enhancer
from starlette.responses import StreamingResponse
import traceback

origins = [
    "http://localhost:8000"
]
MAX_PIXELS = 1e8

enhancer = Enhancer()

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def enhance_image(factor, fast_enhance, input_image, file_name, folder_name, img_type):
    input_folder = "/storage/before/{}".format(folder_name)
    os.makedirs(input_folder, exist_ok=True)
    input_path = os.path.join(input_folder, file_name)
    input_image.save(input_path)
    meta_data = enhancer.enhance(img=input_image,
                                 factor=factor,
                                 img_type=img_type,
                                 fast_enhance=fast_enhance
                                 )
    file_name = re.sub(r'\.[^.]+$', '.jpg', file_name)

    output_folder = "/storage/after/{}".format(folder_name)
    checkfile_folder = "/storage/after/{}/check".format(folder_name)
    os.makedirs(output_folder, exist_ok=True)
    os.makedirs(checkfile_folder, exist_ok=True)

    output_path = os.path.join(output_folder, file_name)
    checkfile_path = os.path.join(checkfile_folder, file_name + '.check')

    meta_data['upscaled_img'].save(output_path)
    open(checkfile_path, 'a').close()

    # del meta_data['upscaled_img']
    # meta_data['output_url'] = output_path[15:]
    # meta_data['factor'] = factor
    # return meta_data


@app.get("/")
def read_root():
    return {"message": "Welcome from the API"}


@app.post("/{type}")
async def get_image(
        background_tasks: BackgroundTasks,
        response: Response,
        factor: float = Form(...),
        folder_name: str = Form(...),
        file_name: str = Form(...),
        img_type: Optional[str] = Form(...),
        fast_enhance: Optional[bool] = Form(...),
        image_file: UploadFile = File(...),
):
    try:
        input_image = Image.open(image_file.file).convert('RGB')
        if input_image is None:
            response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {
                "error": "3",
                "message": "open image file {} failed"}

        w, h = input_image.size
        if w * h * factor * factor > MAX_PIXELS:
            response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {"error": "2",
                    "message": "image too large"}
        background_tasks.add_task(enhance_image,
                                  factor=factor,
                                  fast_enhance=fast_enhance,
                                  input_image=input_image,
                                  file_name=file_name,
                                  folder_name=folder_name,
                                  img_type=img_type)
        response.status_code = status.HTTP_200_OK
        return {"error": "0",
                "message": "enhance job added to background task"}
    except Exception as e:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return {
            "error": "1",
            "message": traceback.format_exc()
        }

    # meta_data = await enhance_image(factor, fast_enhance, file, file_name, folder_name, img_type)
    # return meta_data


def retrieve_img(image_file, image_folder):
    image_path = '../storage/after/{}/{}'.format(image_folder, image_file)
    img = cv2.imread(image_path)
    res, img_encoded = cv2.imencode(".jpg", img)
    res = StreamingResponse(io.BytesIO(img_encoded.tobytes()), media_type="image/jpg")
    return res


def check_file(image_file, image_folder):
    image_checkfile_path = '../storage/after/{}/check/{}.check'.format(image_folder, image_file)
    return os.path.exists(image_checkfile_path)


@app.get("/download/{image_folder}/{image_file}")
def download_image(image_folder: str, image_file: str):
    return retrieve_img(image_file, image_folder)


@app.head("/try/{image_folder}/{image_file}")
def download_image(image_folder: str, image_file: str, response: Response):
    file_ok = check_file(image_file, image_folder)
    if file_ok:
        response.status_code = status.HTTP_200_OK
    else:
        response.status_code = status.HTTP_404_NOT_FOUND


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=7778, workers=12)
