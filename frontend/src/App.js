import React from 'react'
import Header from './parts/header'
import Main from './parts/main'
import Footer from './parts/footer'

function App() {
  return (
    <div>
      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
