const Url = "http://localhost:7778/4x"

export default async function uploadFile(key, file) {
    const formData = new FormData()
    formData.append(key, file)
    return fetch(Url, {
        method: "POST",
        mode: 'no-cors',
        body: formData
    })
    .then(response => {
        if (!response.ok) {
            throw new Error("Network response was not ok")
        }
        return response.json()
    })
    .catch(() => {
        throw new Error("Failed to upload file to " + Url)
    })
} 