import React from 'react'
import Form from 'react-bootstrap/Form'

class ImagePicker extends React.Component {
    onChangeFile = event => {
        if (!event.target.files) return
        event.stopPropagation();
        event.preventDefault();
        if (this.props.onChange) {
            this.props.onChange(event.target.files[0])
        }
    }

    render() {
        return (
            <Form>
                <Form.File 
                    id="custom-file"
                    label="Pick images to get super resolution back"
                    accept="image/*"
                    onChange={this.onChangeFile}
                    disabled={this.props.disabled}
                    custom
                />
            </Form>
        )
    }
}

export default ImagePicker