import React from 'react'

class Dropd extends React.PureComponent {
    state = {
        isOpen: false,
        currentItem: this.props.placeholder
    }

    componentDidUpdate(prevState) {
        if (this.state.currentItem !== prevState.currentItem) {
            this.setState({
                currentItem: this.state.currentItem,
            })
        }
    }

    onItemChange = (item, event) => {
        event.preventDefault()
        event.nativeEvent && event.nativeEvent.stopImmediatePropagation()
        if (item !== this.state.currentItem) {
            console.log("later on...")
        }
    }

    toggelDropdown = event => {
        event.preventDefault()
        event.stopPropagation()

        this.setState(
            ({ isOpen }) => ({ isOpen: !isOpen })
        )
    }

    render() {
        const { items } = this.props
        const className = "dropdown-menu" + this.state.isOpen ? " show" : ""
        return (
            <div className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown"
                     role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                     onClick={event => this.toggelDropdown(event)}>
                    {this.state.currentItem}
                </a>
                <div className={className} aria-labelledby="navbarDropdown" aria-hidden={!this.state.isOpen}>
                    {items && items.map((item, key) => {
                        <li key={key} tabIndex="-1"
                            onMouseDown={event => this.onItemChange(item, event)}>
                        <a tabIndex="-1" className="dropdown-item">
                          {item.label || item}
                        </a>
                      </li>
                    })}
                </div>
            </div>
        )
    }
}

export default Dropd