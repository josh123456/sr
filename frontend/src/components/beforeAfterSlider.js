import React from 'react'
import '../assets/css/beforeAfterSlider.css'

class BeforeAfterSlider extends React.Component {
    constructor(props) {
        super(props)
        this.compare = React.createRef()
    }

    onChange = (event) => {
        this.compare.current.style.width = parseInt(event.target.value) + "%"
    }

    render() {
        return (
            <div className="bas-container">
                <figure style={{backgroundImage: `url(${this.props.originImg})`}}>
                    <div ref={this.compare} className="compare" style={{backgroundImage: `url(${this.props.afterImg})`}}></div>
                </figure>
                <input className="slider" type="range" min="0" max="100" value="50"
                    onInput={this.onChange} onChange={this.onChange}/>
            </div>
        )
    }
}

export default BeforeAfterSlider