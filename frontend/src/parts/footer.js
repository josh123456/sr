import React from 'react'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css';

class Footer extends React.PureComponent {
    render() {
        return (
            <div className="footer">
                <p>This is some content in sticky footer</p>
            </div>
        )
    }
}

export default Footer