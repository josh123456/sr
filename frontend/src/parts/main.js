import React from 'react'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css';
import uploadFile from '../services/ajax'
import Alert from 'react-bootstrap/Alert'
import Container from 'react-bootstrap/Container'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'
import ImagePicker from '../components/imagePicker'
import BeforeAfterSlider from '../components/beforeAfterSlider'
import Spinner from 'react-bootstrap/Spinner'

class Main extends React.Component {
    constructor(props) {
        super(props)
        this.onChange = this.onChange.bind(this)
    }

    state = {
        loading: false,
        error: undefined,
        originImg: "https://webdevtrick.com/wp-content/uploads/1st.jpg",
        afterImg: "https://webdevtrick.com/wp-content/uploads/2nd.jpg"
    }

    async onChange(file) {
        try {
            this.setState({loading: true})
            const response = await uploadFile("originImg", file)
            this.setState({originImg: file, afterImg: response.json()})
        } catch (error) {
            this.setState({loading: false, error: error})
        }
    }

    render() {
        return (
            <div>
                {this.renderIntro()}
                <Container>
                    <ImagePicker onChange={this.onChange} disable={this.state.loading} />
                    {this.renderError()}
                    {this.renderImg()}
                </Container>
            </div>
        )
    }

    renderIntro() {
        return (
            <Jumbotron>
                <h1>Hello, Super Resolution Image</h1>
                <p>
                    In case you struggle with unsatisfied image resolutions, try our machine-learning based <br/> 
                    Image processing tools and get a super resolution image back.
                </p>
                <p>
                    <Button variant="primary">Get Started</Button>
                </p>
            </Jumbotron>
        )
    }

    renderError() {
        if (!this.state.error)
            return null
        return (
            <Alert variant="danger">
                <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                <p>
                    {this.state.error.message}
                </p>
            </Alert>
        )
    }

    renderImg() {
        if (this.state.loading) {
            return (
                <Spinner animation="border" variant="success" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            )
        }
        return <BeforeAfterSlider originImg={this.state.originImg} afterImg={this.state.afterImg} />
    }
}

export default Main