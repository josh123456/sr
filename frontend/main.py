# frontend/main.py

import requests
import streamlit as st
from PIL import Image

# https://discuss.streamlit.io/t/version-0-64-0-deprecation-warning-for-st-file-uploader-decoding/4465
st.set_option("deprecation.showfileUploaderEncoding", False)

# defines an h1 header
st.title("Super Resolution")

# displays a file uploader widget
image = st.file_uploader("Choose an image")

# displays the select widget for the styles
scale = st.selectbox("Choose Upscale", [i for i in ['4x']])

# displays a button
if st.button("Enhance!"):
    if image is not None and scale is not None:
        files = {"file": image.getvalue()}
        res = requests.post(f"http://backend:7778/{scale}", files=files)
        img_path = res.json()
        image = Image.open(img_path.get("name"))
        st.image(image, width=1000)